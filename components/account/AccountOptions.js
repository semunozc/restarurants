import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { map } from "lodash";
import { Icon, ListItem } from "react-native-elements";
import Modal from "../Modal";
import ChangeDisplayNameForm from "./ChangeDisplayNameForm";
import ChangeEmailForm from "./ChangeEmailForm";
import ChangePasswordForm from "./ChangePasswordForm";

export default function AccountOptions({ user, toasRef, setRealoadUser }) {
  const [showModal, setShowModal] = useState(false);
  const [renderComponent, setRenderComponent] = useState();

  const generateOptions = () => {
    return [
      {
        title: "Cambiar nombre y Apellido",
        iconNameLeft: "account-circle",
        iconColorLeft: "#a7bfd3",
        iconNameRight: "chevron-right",
        iconColorRight: "#a7bfd3",
        onpress: () => {
          selectedComponent("diaplayName");
        },
      },
      {
        title: "Cambiar Email",
        iconNameLeft: "at",
        iconColorLeft: "#a7bfd3",
        iconNameRight: "chevron-right",
        iconColorRight: "#a7bfd3",
        onpress: () => {
          selectedComponent("email");
        },
      },
      {
        title: "Cambiar Contraseña",
        iconNameLeft: "lock-reset",
        iconColorLeft: "#a7bfd3",
        iconNameRight: "chevron-right",
        iconColorRight: "#a7bfd3",
        onpress: () => {
          selectedComponent("password");
        },
      },
    ];
  };

  const selectedComponent = (key) => {
    switch (key) {
      case "diaplayName":
        setRenderComponent(
          <ChangeDisplayNameForm
            displayName={user.displayName}
            setShowModal={setShowModal}
            toasRef={toasRef}
            setRealoadUser={setRealoadUser}
          />
        );
        break;
      case "email":
        setRenderComponent(
          <ChangeEmailForm
            email={user.email}
            setShowModal={setShowModal}
            toasRef={toasRef}
            setRealoadUser={setRealoadUser}
          />
        );
        break;
      case "password":
        setRenderComponent(<ChangePasswordForm             
          setShowModal={setShowModal}
          toasRef={toasRef}
          setRealoadUser={setRealoadUser} />
          );
        break;
    }
    setShowModal(true);
  };
  const menuOptions = generateOptions();

  return (
    <View>
      {map(menuOptions, (menu, index) => (
        <ListItem key={index} style={styles.menuItem} onPress={menu.onpress}>
          <Icon
            type="material-community"
            name={menu.iconNameLeft}
            color={menu.iconColorLeft}
          />
          <ListItem.Content>
            <ListItem.Title>{menu.title}</ListItem.Title>
          </ListItem.Content>
          <Icon
            type="material-community"
            name={menu.iconNameRight}
            color={menu.iconColorRight}
          />
        </ListItem>
      ))}
      <Modal isVisible={showModal} setVisible={setShowModal}>
        {renderComponent}
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  menuItem: {
    borderBottomWidth: 1,
    borderBottomColor: "#a7bfd3",
  },
});
