import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button, Icon } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import { isEmpty } from "lodash";

import Loading from "../Loading";
import { validateEmail } from "../../utils/helpers";
import { LoginWithEmailAndPassword } from "../../utils/actions";

const defaultFormValues = () => {
  return { email: "", password: "" };
};

export default function LoginForm() {
  const [showPassword, setShowPassword] = useState(false);
  const [formData, setFormData] = useState(defaultFormValues());

  const [errorEmail, setErrorEmail] = useState("");
  const [errorPasword, setErrorPasword] = useState("");
  const [loading, setLoading] = useState(false);
 
  const navigation = useNavigation();

  const onChange = (e, type) => {
    setFormData({ ...formData, [type]: e.nativeEvent.text });
  };
  const doLogin = async() => {
    if (!validateData()) {
        return;
      }
      setLoading(true)
      const result = await LoginWithEmailAndPassword(formData.email, formData.password);
      setLoading(false)
      if (!result.statusRsponse) {
          setErrorEmail(result.error)
          setErrorPasword(result.error)
          return
      }
      navigation.navigate("account")
  }

  const validateData = () => {
    setErrorEmail("");
    setErrorPasword("");
    let isValid = true;

    if (!validateEmail(formData.email)) {
      setErrorEmail("Debes ingresar un email valido.");
      isValid = false;
    }

    if (isEmpty(formData.password)) {
        setErrorPasword("Debes ingresar su contraseña.");
        isValid = false;
    }
    return isValid;
  };

  return (
    <View style={styles.container}>
      <Input
        onChange={(e) => onChange(e, "email")}
        containerStyle={styles.input}
        placeholder="Ingresa tu email..."
        keyboardType="email-address"
        defaultValue={formData.email}
        errorMessage={errorEmail}
      />
      <Input
        containerStyle={styles.input}
        placeholder="Ingresa tu password..."
        password={true}
        onChange={(e) => onChange(e, "password")}
        secureTextEntry={!showPassword}
        defaultValue={formData.password}
        errorMessage={errorPasword}
        rightIcon={
          <Icon
            type="material-community"
            name={showPassword ? "eye-off-outline" : "eye-outline"}
            iconStyle={styles.icon}
            onPress={() => setShowPassword(!showPassword)}
          />
        }
      />
      <Button
        title="Iniciar Sesion"
        onPress={() => doLogin()}
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
      />
      <Loading isVisible={loading} text="Iniciando Sesion..." />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },
  input: {
    width: "100%",
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
    alignSelf: "center",
  },
  btn: {
    backgroundColor: "#442484",
  },
  icon: {
    color: "#c1c1c1",
  },
});
