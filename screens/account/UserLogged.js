import React, { useState, useRef, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import Toast from "react-native-easy-toast";

import { closeSession, getCurrentUser } from "../../utils/actions";
import Loading from "../../components/Loading";
import InfoUser from "../../components/account/InfoUser";
import AccountOptions from "../../components/account/AccountOptions";

export default function UserLogged() {
  const toasRef = useRef();
  const navigation = useNavigation();

  const [loading, setLoading] = useState(false);
  const [loadingText, setLoadingText] = useState("");
  const [user, setUser] = useState(null);
  const [realoadUser, setRealoadUser] = useState(false)

  useEffect(() => {
    setUser(getCurrentUser());
    setRealoadUser(false)
  }, [realoadUser]);
  return (
    <View style={styles.container}>
      {user && (
        <View>
          <InfoUser
            user={user}
            setLoading={setLoading}
            setLoadingText={setLoadingText}
          />
          <AccountOptions user={user} toasRef={toasRef} setRealoadUser={setRealoadUser} />
        </View>
      )}
      <Button
        buttonStyle={styles.btnClouseSession}
        titleStyle={styles.btnClouseSessionTitle}
        title="Cerrar Sesion"
        onPress={() => {
          closeSession();
          navigation.navigate("restaurants");
        }}
      />
      <Toast ref={toasRef} position="center" opacity={0.9} />
      <Loading isVisible={loading} text={loadingText} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    minHeight: "100%",
    backgroundColor: "#f9f9f9",
  },
  btnClouseSession: {
    marginTop: 30,
    borderRadius: 5,
    backgroundColor: "#fff",
    borderTopWidth: 1,
    borderTopColor: "#442484",
    borderBottomWidth: 1,
    borderBottomColor: "#442484",
    paddingVertical: 10,
  },
  btnClouseSessionTitle: {
    color: "#442484",
  },
});
